
from rest_framework.routers import SimpleRouter
from django.urls import path, include

from api.views import CategoryViewSet, TodoViewSet

router = SimpleRouter()
router.register("category", CategoryViewSet, basename="category")
router.register('todo', TodoViewSet, basename="todo")

urlpatterns = [
    path("", include(router.urls)),
]

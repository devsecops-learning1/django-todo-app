from rest_framework.serializers import ModelSerializer
from core.models import Todo, Category

class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ("id", "name", "created")

class TodoSerializer(ModelSerializer):
    class Meta:
        model = Todo 
        fields = ("id", "title", "category", "completed", "created")

class TodoDetailSerializer(ModelSerializer):
    category = CategorySerializer()
    class Meta:
        model = Todo 
        fields = ("id", "title", "completed", "category", "created")
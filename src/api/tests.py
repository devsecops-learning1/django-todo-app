from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APIRequestFactory

from core.models import Category, Todo

from datetime import datetime

# Create your tests here.

class GlobalViewTestCase(TestCase):

    def setUp(self):
        
        # Création des données pour la catégorie
        self.category_data = {"name": "Category 01"}
        self.category_instance = Category.objects.create(**self.category_data)

        # Création des données de test pour les todos
        self.todo_data = {
            "title": "Todo 01",
            "category": self.category_instance,
            "completed": False
        }
        self.todo_instance = Todo.objects.create(**self.todo_data)

    def test_category(self):
        self.assertEqual(self.category_instance.name, self.category_data['name'])

    def test_todo(self):
        self.assertEqual(self.todo_instance.title, self.todo_data["title"])
        self.assertEqual(self.todo_instance.category, self.todo_data["category"])
        self.assertEqual(self.todo_instance.completed, self.todo_data['completed'])
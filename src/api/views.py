from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet

from core.models import Category, Todo
from .serializers import CategorySerializer, TodoSerializer, TodoDetailSerializer

# Create your views here.

class CategoryViewSet(ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()

class TodoViewSet(ModelViewSet):
    serializer_class = TodoSerializer
    detail_serializer_class = TodoDetailSerializer
    queryset = Todo.objects.all()

    def get_serializer_class(self):
        if self.action == "retrieve":
            return self.detail_serializer_class
        return super().get_serializer_class()